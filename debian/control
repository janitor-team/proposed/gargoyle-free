Source: gargoyle-free
Section: games
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders:
    Sylvain Beucler <beuc@debian.org>,
    Alexandre Detiste <alexandre.detiste@gmail.com>,
Build-Depends:
    cmake,
    libfontconfig1-dev,
    libfreetype6-dev,
    libjpeg-dev,
    libpng-dev,
    libsdl2-dev,
    libsdl2-mixer-dev,
    libspeechd-dev,
    pkg-config,
    qtbase5-dev,
    qtbase5-dev-tools,
    debhelper-compat (= 13)
Standards-Version: 4.6.1
Homepage: https://ccxvii.net/gargoyle/
Vcs-Git: https://salsa.debian.org/games-team/gargoyle-free.git
Vcs-Browser: https://salsa.debian.org/games-team/gargoyle-free
Rules-Requires-Root: no

Package: gargoyle-free
Architecture: any
Depends:
    fonts-go,
    fonts-sil-charis,
    ${shlibs:Depends},
    ${misc:Depends}
Provides: zcode-interpreter, tads2-interpreter, tads3-interpreter
Description: graphical player for Interactive Fiction games
 Gargoyle is an Interactive Fiction (text adventure) player that
 supports all the major interactive fiction formats.
 .
 Most interactive fiction is distributed as portable game files. These
 portable game files come in many formats. In the past, you used to
 have to download a separate player (interpreter) for each format of
 IF you wanted to play. Instead, Gargoyle provides unified player.
 .
 Gargoyle is based on the standard interpreters for the formats it
 supports: .taf (Adrift games, played with Scare), .dat (AdvSys),
 *.agx/.d$$ (AGiliTy), .a3c (Alan3), .asl/.cas (Quest games, played
 with Geas), .jacl/.j2 (JACL), .l9/.sna (Level 9), .mag (Magnetic),
 *.saga (Scott Adams Grand Adventures), .gam/.t3 (TADS),
 *.z1/.z2/.z3/.z4/.z5/.z6/.z7/.z8 (Inform Z-Machine games, played with
 Frotz, Nitfol or Bocfel), .ulx/.blb/.blorb/.glb/.gblorb (Inform or
 Superglús games compiled to the Glulxe VM in Blorb archives, played
 with Git or Glulxe), .zlb, .zblorb (Inform Z-Machine games in Blorb
 archives, played with Frotz).
 .
 (note: do not confuse the Git Glux interpreter with the Git DVCS or
 the GNU Interactive Tools)
 .
 Gargoyle also features graphics, sounds and Unicode support.
 .
 Technically all the bundled interpreters support the Glk API to
 manage I/O (keyboard, graphics, sounds, file) in IF games. Gargoyle
 provides a Glk implementation called garglk that displays texts and
 images in a graphical Qt window, with care on typography.
 .
 Limitations:
 .
 * While Gargoyle can display in-game pictures, it does not provide a
 way to display the cover art present in some Blorb archives.
 .
 * The TADS interpreter doesn't support HTML TADS; you can play
 the games, but will miss the hyperlinks.
